package cz.frantam.benchmark;

import java.io.IOException;

/**
 * @author Franta Mejta
 * @sa.date 2021-02-15T12:37:01+01:00
 */
public class Main {

    public static void main(String[] args) throws IOException {
        org.openjdk.jmh.Main.main(args);
    }

}
