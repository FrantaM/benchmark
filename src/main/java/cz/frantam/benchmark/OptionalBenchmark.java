package cz.frantam.benchmark;

import java.util.Optional;

import org.openjdk.jmh.annotations.Benchmark;

/**
 * @author Franta Mejta
 * @sa.date 2021-02-15T12:38:08+01:00
 */
public class OptionalBenchmark {

    private Response getNullResponse() {
        return Response.nullResponse;
    }

    private Response getNonNullResponse() {
        return Response.nonNullResponse;
    }

    @Benchmark
    public Object ofNullableWithNull() {
        Response response = getNullResponse();
        return Optional.ofNullable(response).map(Response::getValue);
    }

    @Benchmark
    public Object ofNullableWithNonNull() {
        Response response = getNonNullResponse();
        return Optional.ofNullable(response).map(Response::getValue);
    }

    @Benchmark
    public Object conditionWithNull() {
        Response response = getNullResponse();
        return response != null ? Optional.of(response.getValue()) : Optional.empty();
    }

    @Benchmark
    public Object conditionWithNonNull() {
        Response response = getNonNullResponse();
        return response != null ? Optional.of(response.getValue()) : Optional.empty();
    }

    @SuppressWarnings("FieldMayBeFinal")
    private static class Response {

        private static Response nullResponse = null;
        private static Response nonNullResponse = new Response(new Object());
        private final Object value;

        private Response(Object value) {
            this.value = value;
        }

        public Object getValue() {
            return this.value;
        }

    }

}
